module.exports = {
  title: 'Check On Site',
  description: 'Documentación del proyecto',
  themeConfig: {
    nav: [{ text: 'Inicio', link: '/' }],
    sidebar: [
      ['/', 'Inicio'],
      ['/installation/', 'Instalación'],
      ['/technical_manual/', 'Manual de Usuario']
    ]
  }
}
