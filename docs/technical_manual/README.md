# Manual Técnico


## Ingreso
> `/admin/login`

Debe ingresar el correo electrónico y la contraseña establecidas para el administrador.

![Creador de Plantillas](../images/1.jpg)



## Dashboard
> `/admin`

Una vez haya iniciado sesión el sistema lo redirigirá al dashboard en donde encontrará el munú y estadísticas generales de la plataforma.

![Creador de Plantillas](../images/2.png)

A través del menu podrá gestionar los módulos de la plataforma

| Módulos - Menu     |      |
| ------------- | :------------: |
| Usuarios       | :heavy_check_mark: |
| Grupos       | :heavy_check_mark: |
| Plantillas  | :heavy_check_mark: |
| Inspecciones | :heavy_check_mark: |
| Acciones | :heavy_check_mark: |
| Cronograma | :heavy_check_mark: |

## Usuarios

> `admin/cms_users`

Para este sistema en particular los usuarios son sincronizados desde el directorio activo, es importante evitar eliminar y/o crear usuarios manualmente, un situación puntual puede ser habilitar a un usuario existente como administrador.


![Creador de Plantillas](../images/3.jpg)


## Grupos
> `admin/groups`

A través de este módulo puede gestionar y asociar varios usuarios, lo cual le será útil al momento de asignar y programar inspecciones.


![Creador de Plantillas](../images/4.jpg)

## Plantillas
> `admin/templates`

Este quiza sea el módulo más relevante dentro de la plataforma, como Administrador puede gestionar las plantillas que los usuarios luego podrán usar en la aplicación móvil.

![Creador de Plantillas](../images/5.jpg)

### Creador de Plantillas
> `admin/templates/add`

En esta área usted puede crear nuevas plantillas, arrastrando y soltando elementos del listado izquierdo hacia la parte central.

![Creador de Plantillas](../images/template_creator.gif)


| Items Disponibles   |   Descripción   |
| :------------- | :------------ |
| Categoría       | ![Categoria](../images/forms/category.png) Puede agrupar ítems |
| Texto Simple       | ![Categoria](../images/forms/enlarge-text.png) Entrada de texto simple |
| Texto Multilinea  | ![Texto](../images/forms/text-align-left.png) Entrada de textos grandes |
| Switch | ![Slider](../images/forms/switch.png) Entrada tipo swith (verdadero - falso) |
| Selección múltiple | ![Check Box](../images/forms/check-box.png) Entrada de selección múltiple ó única |
| GPS | ![GPS](../images/forms/gps.png) Entrada para compartir geolocalización |
| Video | ![Video](../images/forms/play.png) Entrada de video(s) |
| Imagen | ![Imagen](../images/forms/photography.png) Entrada de imagen(es) |
| Slider | ![Slider](../images/forms/slider.png) Entrada de números|
| Fecha/ Hora | ![Calendario](../images/forms/calendar.png) Entrada de periodo de tiempo |


## Inspecciones
> `admin/inspections`

<p>En el área de inspecciones puede gestionar todas las inspecciones creadas por los usuarios de la aplicación móvil. En el detalle de cada inspección podrá ver la información de la programación junto con las respuestas hechas por el usuario, los planes de acción, las anotaciones y las imagenes adjuntas.

![Inspecciones](../images/lista_inspecciones.gif)

#### Información

Se observa si es una inspección programada o no, el usuario que la ha creado, la fecha de última actualización, el estado de la inspección y a que plantilla pertenece la inspección.

#### Respuestas

En esta área podrá ver la plantilla de la inspección junto con las respuestas que haya ingresado el usuario.

#### Acciones adjuntas

En esta área podrá ver las acciones adjuntas en cada pregunta, puede ver la relación con cada pregunta. [Más Información](#planes-de-accion)

#### Notas

Las notas son elementos que el usuario que responda a una pregunta puede adjuntar si asi lo considera.

#### Imágenes

En esta área puede ver las imágenes que fueron adjuntadas a cada pregunta.

</p>




## Planes de Acción
> `admin/actions`

Un plan de acción es una acción que un usuario puede adjuntar a una respuesta cuando responde una inspección, particularmente sirve para delegar tareas especificas a un usuario. Desde el administrador usted puede ingresar comentarios, una vez registrado el comentario se le enviara una notificación al usuario asignado a dicha tarea.


![Acciones](../images/plan_action_1.gif)

## Cronograma


![Acciones](../images/cronograma_1.gif)
En el módulo de cronograma usted podrá gestionar la programación de las inspecciones, puede cambiar la vista de inspecciones entre programación diaria, semanal o mensual.

<p>
<img src="../images/view_programa.gif" align="left" width="400px" style="margin-right:10px" />
Haciendo clic sobre un evento dentro del calendario usted podrá ver el detalle de cada programación.
 </p>
<hr style="clear:both">	

#### Programar una Inspección

<p>

<img src="../images/programar_1.gif" align="right" width="400px" style="margin-left:10px" />
para generar una nueva programación debe dar cilc en el botón verde en la parte superior derecha, sedesplegará una ventana que le solicitara datos elementales para poder crear una nueva programación.
 
**Título**: Este campo es necesario para diferenciar rapidamente el objetivo de la programación
 
**Plantilla**: La plantilla es el formulario base donde se completará la inspección.

#### Asignar Inspección

Usted puede asignar una programación a usuarios en particular y/o grupos, en la imagen de ejemplo solo se le es asignado a un usuario.

**Observación**: Este campo es opcional y puede colocar en él la información que considere necesaria.

**Repetición**: Usted puede seleccionar entre dos opcines. Si la inspección que esta programando solo se realizará una vez o si en cambio esta programación se repite.

**Disponibilidad**: Si usted ha seleccionado que la inspección solo se realice una vez, este campo de texto será visible, y debera llenarlo con dos fechas (fecha y hora) en las que indicará el inicio (desde cuando estará habilitado) y el final (fecha máxima).

**Tipo de Repetición**: Si ha seleccionado "repetir inspección", se mostrará un campo para seleccionar el tipo de repetición que desea.

| Tipos de Repetición |
| :------------: |
| Diario |
| Semanal |
| Quincenal |
| Mensual |


</p>

<hr style="clear:both">	

