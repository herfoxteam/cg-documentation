---
title: Home
meta:
  - name: description
    content: Page description
  - name: keywords
    content: js vuejs vuepress
---

# Check On Site

> Versión 1.0.0

<p align="center">
  <img src="./images/check_on_site.png" width="280px" />
</p>

[[toc]]


## Descripción

Sistema de gestión de inspecciones y planes de acción que cuenta con un administrador web (API) y una aplicación móvil.



## Requisitos Tecnicos 

### PHP >= 7.1.4

| Extensión     |      |
| ------------- | :------------: |
| OpenSSL       | :heavy_check_mark: |
| PDO PHP       | :heavy_check_mark: |
| Mbstring PHP  | :heavy_check_mark: |
| Tokenizer PHP | :heavy_check_mark: |
| XML PHP       | :heavy_check_mark: |
| Ctype PHP     | :heavy_check_mark: |
| JSON PHP      | :heavy_check_mark: |
| MYSQLND       | :heavy_check_mark: |
| LDAP          | :heavy_check_mark: |
| GD            | :heavy_check_mark: |
| CURL          | :heavy_check_mark: |
| PDO_SQLSRV    | :heavy_check_mark: |
| XSL           | :heavy_check_mark: |

### Base de Datos

| Base de Datos | |
| ------------- | ---- |
| SQL Server    |   |



