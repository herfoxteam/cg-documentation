# Instalación


## Instalar Git

Git es un software de control de versiones, debe instalarse para clonar el codigo del proyecto en el servidor.
[GIT](https://git-scm.com/)

## Instalar Composer

Composer es un manejador de paquetes de PHP [Composer](https://getcomposer.org/)


## Clonar repositorio

El sistema debe ser copiado directamente desde el repositorio original del proyecto. Abra una terminal en la raiz del directorio especificado para el proyecto y escriba el siguiente comando.

``` git
git clone https://camoran@bitbucket.org/herfoxteam/cg-inspecciones-api.git .
```

## Instalar Dependencias

Debe instalar a través de composer las dependecias del proyecto. Para ello debe ejecutar el siguiente comando en la raiz del proyecto.

``` 
composer install
```

## Variables de Entorno

El archivo con las variables de entorno, es un documento con las variables de conexión y configuración del sistema, sin este archivo no podrá ejecutar la aplicación, para tal caso debe crear un archivo .env en la raiz del proyecto y copiar en él la siguiente información.

```
APP_NAME=CheckOnSite
APP_ENV=local
APP_KEY=base64:ikBQboMQcNksIaD27hKlSR/ZdRMsHU9KAKUAZthLM7c=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=single

DB_CONNECTION=sqlsrv
DB_HOST=
DB_PORT=1433
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

FILESYSTEM_CLOUD=s3
FILESYSTEM_DRIVER=local
AWS_ACCESS_KEY_ID=AKIAI5KBYHYINERK7E4Q
AWS_SECRET_ACCESS_KEY=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
AWS_DEFAULT_REGION=us-east-2
AWS_BUCKET=herfoxdev
AWS_URL=https://s3.us-east-2.amazonaws.com/herfoxdev/

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
SESSION_LIFETIME=120
QUEUE_DRIVER=database

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=465
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=ssl
MAIL_FROM_ADDRESS=
MAIL_FROM_NAME=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

L5_SWAGGER_GENERATE_ALWAYS=true

SQS_KEY=AKIAIUEHICWFLTAYIHKA
SQS_SECRET=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
SQS_PREFIX=https://sqs.us-east-2.amazonaws.com/050110842340
SQS_QUEUE=compass_inspecciones
SQS_REGION=us-east-2

FCM_SERVER_KEY=AAAAhEQ0Hxs:APA91bF4gF8kuSHX4FZAezY9famTw2zdiz5FSB8imt9OVPZqos3ZEs7NpWk0rbPZ6Np09hxq_jKTrNZvQH4FGXg6VP-GYEeVc-HrmjoUlRUkO9Uya0gq_zsYgj1Pz2qIc45YhO1gANUT
FCM_SENDER_ID=568079949595

``` 

Ahora debe configurar los datos de conexión a la base de datos en el archivo .env

| Variables     |      |
| ------------- | :------------: |
| DB_HOST       | :heavy_check_mark: |
| DB_PORT       | :heavy_check_mark: |
| DB_DATABASE  | :heavy_check_mark: |
| DB_USERNAME | :heavy_check_mark: |
| DB_PASSWORD | :heavy_check_mark: |